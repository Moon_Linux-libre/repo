#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Tools to control code flow (errors etc.)
# Copyright (C) 2022 Lia Va


[ -z "$LIBMOONDEVTOOLS_CONTROL_FLOW_HELPERS_SH" ] || return
LIBMOONDEVTOOLS_CONTROL_FLOW_HELPERS_SH=1


die() {
    [ $# = 0 ] || error "$@"
    exit 1
}

ignore_error() {
    "$@" 2>/dev/null
    return 0
}

match() {
	eval "case '$1' in $2) ;; *) false;; esac"
}

exists() {
	ls $1 >/dev/null 2>&1
}

filter() {
	local -a res=()
	local f="$1"
	shift

	while [ $# != 0 ]; do
		eval "$f" && res+=("$1")
		shift
	done

	printf '%q ' "${res[@]}"
}
