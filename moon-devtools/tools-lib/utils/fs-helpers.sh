#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Tools to interact with file systems
# Copyright (C) 2022 Lia Va


[ -z "$LIBMOONDEVTOOLS_FS_HELPERS_SH" ] || return
LIBMOONDEVTOOLS_FS_HELPERS_SH=1

MOON_LIBRARY=${MOON_LIBRARY:-'/usr/share/moon-devtools'}

source "$MOON_LIBRARY/utils/control-flow-helpers.sh"


findback() {
	local path=`realpath "$1"`
	shift

	local first_only=false
	match "$*" '*\ -quit\ *|-quit\ *|*\ -quit|-quit' && first_only=true

	local out=''
	while [ -n "$path" ]; do
		res=`find "$path"/ -maxdepth 1 "$@"`
		path="${path%/*}"

		[ -n "$res" -a -n "$out" ] && out+=$'\n'
		out+="$res"
		$first_only && [ -n "$out" ] && break
	done
	printf '%s' "$out"
}
