#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Header to initialize CLI tool file
# Copyright (C) 2022 Lia Va


[ -n "$LIBMOONDEVTOOLS_CLI_HEADER_SH" ] && return
LIBMOONDEVTOOLS_CLI_HEADER_SH=1


. /usr/share/makepkg/util.sh

export LANG=C

if [ -t 2 -a "$TERM" != dumb ]; then
    colorize
else
    # shellcheck disable=2034
    declare -gr ALL_OFF='' BOLD='' BLUE='' GREEN='' RED='' YELLOW=''
fi
