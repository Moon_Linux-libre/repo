#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Tools to run script as root
# Copyright (C) 2022 Lia Va


[ -z "$LIBMOONDEVTOOLS_ROOT_SCRIPT_SH" ] || return
LIBMOONDEVTOOLS_ROOT_SCRIPT_SH=1


orig_argv=("$0" "$@")
root_script() {
    local keepenv="$1"

    [ $EUID -eq 0 ] && return
    if type -P sudo >/dev/null; then
        exec sudo --preserve-env="$keepenv" -- "${orig_argv[@]}"
    else
        exec su root -c "$(printf ' %q' "${orig_argv[@]}")"
    fi
}
