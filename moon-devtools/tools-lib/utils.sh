#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-3.0-or-later
# Utils lib include entry point
# Copyright (C) 2022 Lia Va


[ -z "$LIBMOONDEVTOOLS_UTIL_SH" ] || return
LIBMOONDEVTOOLS_UTIL_SH=1

MOON_LIBRARY=${MOON_LIBRARY:-'/usr/share/moon-devtools'}

for lib in "$MOON_LIBRARY/utils/"*.sh; do
    source "$lib"
done
